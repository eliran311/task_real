<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\user;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $customers = Customer::all();
        return view('customers.index', compact('customers'));
        
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer= new Customer();
        $id =Auth::id();//the idea of the current user
        $customer->user_id = $id;
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->status = 0;
       
        $customer->save();
        return redirect('customers');
 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)//ביצוע שאלה 5 א בנוסף. מניעת עריכה ממי שהלקוח לא שייך אליו ואם הוא לא מנהל
    {
        $customer = Customer::find($id);
        if (Gate::denies('manager')) {
            if ($id<>user_id){
            abort(403,"you can't edit");}}
        return view('customers.edit', compact('customer'));


    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);
       
        $id1=Auth::id();
        $customer->user_id=$id1;
        $customer->update($request->except(['_token'])); 
        return redirect('customers');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");}
        
            $customer = Customer::findOrFail($id);
            $customer->delete();
            return redirect('customers');
        }
        public function buying($id)   //פונקציה לשינוי הסטטוס
    {
        if (Gate::denies('manager')) {
            abort(403,"you cant mark");}
        $customer = Task::findOrFail($id);
        $customer->status = 1;
        $customer->save();
        return redirect('customers');
    }

    
    }

