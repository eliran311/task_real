@extends('layouts.app')

@section('content')

<form method="post"  action="{{action('TaskController@update', $customer->id) }}">
 @method('PATCH')
 @csrf
 <div class="form-group">
   <label for="item">Update name</label>
   <input type="text" class="form-control" name="title" value="{{ $customer->name}}" />
   <label for="item">Update email</label>
   <input type="text" class="form-control" name="title" value="{{ $customer->email}}" />
   <label for="item">Update phone</label>
   <input type="text" class="form-control" name="title" value="{{ $customer->phone}}" />
 </div>
 <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection

